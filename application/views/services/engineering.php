<section class="page-banner" style="background-image:url(<?= base_url('assets/images/background/engineering_and_tech_2.jpg') ?>); ">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Engineering and Technical Services  </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
					<li><a href="javascript:;">Services</a></li>
                    <li>Engineering and Technical Services </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International Limited</div>
									<h2>Engineering and Technical Services</h2>
								</div>
								<div class="bold-text">Our unique skilled personnel allow us to provide wide range of engineering and technical support for oil and gas industry. Our engineering and technical support services covering both offshore and onshore include:</div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img  class="mb-2" src="<?= base_url('assets/images/services/engineering.jpg') ?>" alt="" />
                                                    <img  class="mb-2" src="<?= base_url('assets/images/services/clean_up.jpg') ?>" alt="" />
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <ul class="list-style-one">
                                                    <li>Installations of Pipeline & Storage Tanks</li>
                                                    <li>Process Plant Construction & Modernisation</li>
                                                    <li>Equipment Installation, Maintenance and Upgrade</li>
                                                    <li>Upgrading or Revamping Projects on existing systems</li>
                                                    <li>Technical Support and Assistance</li>
                                                    <li>Rotating Equipment Installation, Maintenance and Upgrade</li>
                                                    <li>Installation and Maintenance of Electrical Work</li>
                                                    <li>Maintenance Inspection and Engineering</li>
                                                    <li>Valve and Pumps Repair and Maintenance</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <h3> Other Technical Services</h3>
                                    <p>HRI also design, fabricates and installs all types of pipe work, tanks and structural steelworks ranging from individual components to complete processing system.</p>
                                    
                                </div>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>