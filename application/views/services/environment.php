<section class="page-banner" style="background-image:url(<?= base_url('assets/images/background/environment_restoration.jpg') ?>);background-position:25% 20%;">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Environment Restoration Services </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
					<li><a href="javascript:;">Services</a></li>
                    <li>Environment Restoration Services </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
                        <div class="row">
                            <!-- Services Block Three -->
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/oilspill-cleanup-and-recovery') ?>"><img src="<?= base_url('assets/images/services/oil-spill.jpg') ?>" style="height:250px"  alt="" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/oilspill-cleanup-and-recovery') ?>">Oil Spill Clean-up And Recovery</a></h3>
                                        <a href="<?= base_url('services/oilspill-cleanup-and-recovery') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/remediation-and-rehabilitation') ?>"><img src="<?= base_url('assets/images/services/rmediation-2.jpg') ?>" style="height:250px" alt="" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/remediation-and-rehabilitation') ?>">Remediation And Rehabilitation Of Past Impacted Site</a></h3>
                                        <a href="<?= base_url('services/remediation-and-rehabilitation') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/oilspill-response-equipment') ?>"><img src="<?= base_url('assets/images/services/spill-equipment.jpg') ?>" style="height:250px" alt="" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/oilspill-response-equipment') ?>">Oil Spill Response Equipment and Material Supplies</a></h3>
                                        <a href="<?= base_url('services/oilspill-response-equipment') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/oil-waste-handling') ?>"><img src="<?= base_url('assets/images/services/oily-waste.jpeg') ?>" style="height:250px" alt="" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/oil-waste-handling') ?>">Oily Waste Handling And Management From Collection To Disposal</a></h3>
                                        <a href="<?= base_url('services/oil-waste-handling') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>                
                        </div>
					</div>
				</div>
				
			</div>
		</div>
	</div>