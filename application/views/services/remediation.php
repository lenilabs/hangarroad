<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/rehabilitation.jpg') ?>);background-position:25% 60%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1 style="font-size:45px;">Remediation And Rehabilitation Of Past Impacted Site </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/environment') ?>">Enviromental Restoration Services</a></li>
                    <li>Remediation And Rehabilitation Of Past Impacted Site</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Remediation And Rehabilitation Of Past Impacted Site</h2>
                                </div>

								<div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Contaminated site clean-up</h3>
                                                    <p>Site clean-up works include testing, excavation, emu picking, loading, transport and disposal of materials/soil from Class 1 to Class 5.</p>
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img src="<?= base_url('assets/images/services/rmediation-2.jpg') ?>" />
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <ul class="list-style-one">
                                                    <li>Fly tipping impacted sites which may have a full range of waste materials including household waste, commercial and industrial waste, hazardous materials including asbestos, chemicals and oils.</li>
                                                    <li>Asbestos impacted sites from previous operations and demolition works by others.</li>
                                                    <li>Hazardous materials impacted sites including pesticides, heavy metals, hydrocarbons and chemicals.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <p>We have successfully completed a number of projects requiring the establishment and management of soil bio-piles to allow the contaminated soil to be reused onsite rather than landfill the soil and replace with clean fill. Works include backfilling and compacting the remediated soil.</p>
                                    
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Reinstatement of services and landscaping</h3>
                                                    <p>Our in-house and sub-contractor resources allow us to install or reinstate services and/or landscaping removed or impacted during works.</p>
                                                </div>
                                            </div>
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Recovery of damaged and impacted land for re-use and redevelopment cost effectively</h3>
                                                    <p>Through our operations and expertise we can work with clients to develop plans and methodologies that allow impacted land to be remediated and recovered for reuse at the original or a higher end land use. These works can be done on a contract basis a joint venture basis or a purchase basis.</p>
                                                    <p>We work with clients to achieve necessary approvals for redevelopment of the site.</p>
                                                    <p>We can project manage the development works and carry out the earthworks/landscaping for sub divisions.</p>
                                                    <p>Typical sites that can be redeveloped included former quarries, clay pits, sand pits, industrial sites, commercial sites and residential sites where there has been an adverse impact from previous operations.</p>
                                                </div>
                                            </div>
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Hazardous Materials Removal</h3>
                                                    <p>Services cover SMF, lead, PCB’s, refrigerants and hazardous materials such as creosote and chemicals among other things. We identify, test, collect and dispose of hazardous materials from Class 1 to Class 5.</p>
                                                </div>
                                            </div>
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Working with Environmental Consultants to provide a clean usable site for clients</h3>
                                                    <p>We work with a number of environmental consultants to assist them in achieving the required outcomes for their clients land.</p>
                                                    <p>Works cover the full range of our operational services.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>