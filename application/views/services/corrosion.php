<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/pipe-coating.jpg') ?>);background-position:25% 30%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Corrosion Prevention and Control Services </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/facility') ?>">Facility Maintenance</a></li>
                    <li>Corrosion Prevention and Control Services</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Corrosion Prevention and Control Services</h2>
                                </div>
                                <div class="bold-text">Hangar Road International Limited is specialised in the survey, design, installation, application and maintenance of all kinds of cathodic protection systems for the protection of buried and submerged structures, as well as structures containing imbedded-in and/or in-contact-with corrosive electrolytes. Our mission is to heal and protect assets from the effects of corrosion, and to enhance life. Cathodic protection systems are most commonly used to protect steel, water or fuel pipelines and storage tanks, steel pier piles, ships, offshore oil platforms and onshore / offshore oil / gas / water well casings. Our services include, but are not limited to: Conducting feasibility studies and CP assessments, Designing and installing CP, Surveying existing and/or new facilities and performing annual surveys, in addition to completing preventive maintenance checks and offering a wide spectrum of CP materials, equipment’s and tool.</div>

								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>