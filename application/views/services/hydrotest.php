<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/hydro-test-long.jpg') ?>);background-position:25% 60%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Hydro Test </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/facility') ?>">Facility Maintenance</a></li>
                    <li>Hydro Test</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Hydro Test</h2>
                                </div>
                                <div class="bold-text">Hydrostatic (Hydro) Testing is a process where components such as piping systems, gas cylinders, boilers, and pressure vessels are tested for strength and leaks. Hydro tests are often required after shutdowns and repairs in order to validate that equipment will operate under desired conditions once returned to service.</div>

								<div class="bold-text">Furthermore, hydrostatic testing cannot be performed during normal operations and cannot monitor equipment for leaks after the test has been performed. On-stream equipment integrity is best managed by an effective fixed equipment mechanical integrity program. 
                                    <br />Although hydrostatic testing is considered to be a nondestructive testing method, equipment can rupture and fail if the inspection exceeds a specified test pressure or if a small crack propagates rapidly.</div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="">
                                                    <h3>How does it work?</h3>
                                                    <p>Hydrostatic testing is a type of pressure test that works by completely filling the component with water, removing the air contained within the unit, and pressurizing the system up to 1.5 times the design pressure limit the of the unit. The pressure is then held for a specific amount of time to visually inspect the system for leaks. Visual inspection can be enhanced by applying either tracer or fluorescent dyes to the liquid to determine where cracks and leaks are originating.</p>
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image" style="padding-top:60px">
													<img src="<?= base_url('assets/images/services/hydrotest.jpg') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Common Methods </h3>
                                    <p>There are three common hydrostatic testing techniques that are used to test small pressure vessels and cylinders: the water jacket method, the direct expansion methodm, and the proof testing method.</p>
                                    <div class="services-featured-outer">
										<div class="row clearfix">
											
											<!-- Feature Block -->
											<div class="feature-block col-sm-12">
												<div class="inner-box">
													<h4>Water Jacket Method</h4>
													<div class="featured-text">
                                                        <p>In order to conduct this method, the the vessel is filled with water and loaded it into a sealed chamber (called the test jacket) which is also filled with water. The vessel is then pressurized inside the test jacket for a specified amount of time. This causes the vessel to expand within the test jacket, which results in water being forced out into a glass tube that measures the total expansion. Once the total expansion is recorded, the vessel is depressurized and shrinks to its approximate original size. As the vessel deflates, water flows back into the test jacket.</p>
                                                        
                                                        <p>Sometimes, the vessel does not return to its original size. This second size value is called permanent expansion. The difference between the total expansion and permanent expansion determines whether or not the vessel is fit-for service. Typically the higher the percent expansion, the more likely the vessel will be decommissioned.</p>
                                                    </div>
												</div>
											</div>
											
											<!-- Feature Block -->
											<div class="feature-block col-sm-12">
												<div class="inner-box">
													
													<h4>Direct Expansion Method</h4>
													<div class="featured-text">The direct expansion method involves filling a vessel or cylinder with a specified amount of water, pressurizing the system, and measuring the amount of water that is expelled once the pressure is released. The permanent expansion and the total expansion values are determined by recording the amount of water forced into the vessel, the test pressure, and amount of water expelled from the vessel.</div>
												</div>
											</div>
											
											<!-- Feature Block -->
											<div class="feature-block col-sm-12">
												<div class="inner-box">
													<h4>Proof Pressure Method</h4>
													<div class="featured-text">The proof pressure test applies an internal pressure and determine if the vessel contains any leaks or other weakness such as wall thinning that may result in failure.1 In the United States, This method is only permitted when the U.S. Code of Federal Regulations does not require permanent and total expansion values to be recorded.</div>
												</div>
                                            </div>
                                            
                                            <div class="feature-block col-sm-12">
												<div class="inner-box">
													<h4>Alternative Methods</h4>
													<div class="featured-text">Some equipment may not be designed to handle the loads required for a pressure test. In these cases, alternative methods such as pneumatic testing should be employed. Pneumatic testing is another type of pressure test that involves pressurizing the vessel with a gas such as air or nitrogen instead of water. However, special caution should be used when performing pneumatic testing as gaseous mediums have the ability to be compressed and contained in larger amounts compared to hydrostatic testing.</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>