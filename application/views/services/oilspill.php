<section class="page-banner" style="background-image:url(<?= base_url('assets/images/background/environment_restoration.jpg') ?>);background-position:25% 60%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Oil Spill Clean-up And Recovery </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/environment') ?>">Environmental Restoration Services</a></li>
                    <li>Oil Spill Clean-up And Recovery</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Oil Spill Clean-up And Recovery</h2>
                                </div>
                                <div class="bold-text">No two oil spills are the same because of the variation in oil types, locations, and weather conditions involved. However, broadly speaking, there are four main methods of response we employ.</div>

                                <div class="services-featured-outer">
                                    <div class="row clearfix">
                                        
                                        <!-- Feature Block -->
                                        <div class="feature-block col-sm-12">
                                            <div class="inner-box">
                                                <div class="icon-box">
                                                    <span class="icon"><img src="<?= base_url('assets/images/icons/one.png') ?>" width="64" /></span>
                                                </div>
                                                <div class="featured-text">
                                                    <p>Leave the oil alone so that it breaks down by natural means. If there is no possibility of the oil polluting coastal regions or marine industries, the best method is to leave it to disperse by natural means. A combination of wind, sun, current, and wave action will rapidly disperse and evaporate most oils. Light oils will disperse more quickly than heavy oils.</p>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-block col-sm-12">
                                            <div class="inner-box">
                                                <div class="icon-box">
                                                    <span class="icon"><img src="<?= base_url('assets/images/icons/two.png') ?>" width="64" /></span>
                                                </div>
                                                <div class="featured-text">
                                                    <p>Contain the spill with booms and collect it from the water surface using skimmer equipment. Spilt oil floats on water and initially forms a slick that is a few millimeters thick. There are various types of booms that can be used either to surround and isolate a slick, or to block the passage of a slick to vulnerable areas such as the intake of a desalination plant or fish-farm pens or other sensitive locations. Boom types vary from inflatable neoprene tubes to solid, but buoyant material. Most rise up about a meter above the water line. Some are designed to sit flush on tidal flats while others are applicable to deeper water and have skirts which hang down about a meter below the waterline. Skimmers float across the top of the slick contained within the boom and suck or scoop the oil into storage tanks on nearby vessels or on the shore. However, booms and skimmers are less effective when deployed in high winds and high seas.</p>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-block col-sm-12">
                                            <div class="inner-box">
                                                <div class="icon-box">
                                                    <span class="icon"><img src="<?= base_url('assets/images/icons/three.png') ?>" width="64" /></span>
                                                </div>
                                                <div class="featured-text">
                                                    <p>Use dispersants to break up the oil and speed its natural biodegradation. Dispersants act by reducing the surface tension that stops oil and water from mixing. Small droplets of oil are then formed, which helps promote rapid dilution of the oil by water movements. The formation of droplets also increases the oil surface area, thus increasing the exposure to natural evaporation and bacterial action. Dispersants are most effective when used within an hour or two of the initial spill. However, they are not appropriate for all oils and all locations. Successful dispersion of oil through the water column can affect marine organisms like deep-water corals and sea grass. It can also cause oil to be temporarily accumulated by subtidal seafood. Decisions on whether or not to use dispersants to combat an oil spill must be made in each individual case. The decision will take into account the time since the spill, the weather conditions, the particular environment involved, and the type of oil that has been spilt.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="feature-block col-sm-12">
                                            <div class="inner-box">
                                                <div class="icon-box">
                                                    <span class="icon"><img src="<?= base_url('assets/images/icons/four.png') ?>" width="64" /></span>
                                                </div>
                                                <div class="featured-text">
                                                    <p>Introduce biological agents to the spill to hasten biodegradation. Most of the components of oil washed up along a shoreline can be broken down by bacteria and other microorganisms into harmless substances such as fatty acids and carbon dioxide. This action is called biodegradation. The natural process can be speeded up by the addition of fertilizing nutrients like nitrogen and phosphorous, which stimulate growth of the microorganisms concerned. However the effectiveness of this technique depends on factors such as whether the ground treated has sand or pebbles and whether the fertilizer is water soluble or applied in pellet or liquid form.</p>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>