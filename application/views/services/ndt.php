<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/ndt-long.jpg') ?>);background-position:25% 20%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Non Destructive Testing (NDT) </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/facility') ?>">Facility Maintenance</a></li>
                    <li>Non Destructive Testing</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Non Destructive Testing</h2>
                                </div>
                                <div class="bold-text">We help you ensure the reliability and safety of your products, equipment or plant assets with our world-class services in non-destructive testing (NDT), materials testing and welding quality.  We understand the processes involved throughout the entire product cycle – from design and manufacture to operation and maintenance - and the frequent need for rapid turnaround times. Our Total Quality Assurance services can support production processes, quality control, and regulatory compliance as well as new construction, pipelines, plant maintenance and scheduled shutdown inspection.</div>

								<div class="bold-text">Our vast expertise and knowledge in NDT and materials testing means we can select the right techniques and procedures to detect defects and irregularities in your products, equipment, production facilities or plant assets and provide you with necessary data to assist you in making informed decisions.  HRI can help you avoid the potential for catastrophic consequences and financial losses with early detection of problems before they cause damage, operating inefficiencies or in-service failure.</div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img src="<?= base_url('assets/images/services/ndt-ultrasonic.jpg') ?>" style="height:250px;" />
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="bold-text">
                                                    In addition to our non-destructive testing and materials testing, we provide a complete welding support service to help you with your welding requirements, comply with international and national codes and standards, and to enter new markets with our welder training and welder qualification.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>The benefits of choosing HRI for non-destructive testing, materials testing and welding </h3>
                                    <p>By partnering with HRI, we will deliver real value for your company by protecting your brand and reputation and by ensuring the safe and efficient operation of your products, equipment and plant assets.  We help you save time and money by avoiding the costs and loss of revenue due to product, equipment and asset defects or failure, all done with the minimum of disruption to your production processes and schedules.</p>

                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>