<?php $controller = $this->uri->segment(1); ?>
                            <ul class="navigation clearfix">
                                <li class="<?= ($controller == '' || $controller == 'welcome') ? 'current' : ''; ?>"><a href="<?= base_url() ?>">Home</a>
                                </li>
                                <li class="<?= ($controller == 'about') ? 'current' : ''; ?>"><a href="<?= base_url('about') ?>">About us</a></li>
                                <li class="dropdown <?= ($controller == 'services') ? 'current' : ''; ?>"><a href="<?= base_url('services') ?>">Services</a>
                                    <ul style="right:0px;left:-170px;">
                                        <!-- <li><a href="<?//= base_url('services/oil_and_gas') ?>">Oil and Gas</a></li> -->
                                        <li class="dropdown">
                                            <a href="<?= base_url('services/environment') ?>">Environment Restoration Services</a>
                                            <ul>
                                                <li><a href="<?= base_url('services/oilspill-cleanup-and-recovery') ?>">Oil Spill Clean-up And Recovery</a></li>
                                                <li><a href="<?= base_url('services/remediation-and-rehabilitation') ?>">Remediation And Rehabilitation Of Past Impacted Site     </a></li>
                                                <li><a href="<?= base_url('services/oilspill-response-equipment') ?>">Oil Spill Response Equipment and Material Supplies</a></li>
                                                <li><a href="<?= base_url('services/oil-waste-handling') ?>">Oily Waste Handling And Management From Collection To Disposal</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown right">
                                            <a href="<?= base_url('services/facility') ?>">Facility Maintenance</a>
                                            <ul>
                                                <li><a href="<?= base_url('services/pipeline') ?>">Pipeline Maintenance</a></li>
                                                <li><a href="<?= base_url('services/pipeline-pigging') ?>">Pipeline Pigging and Inspection</a></li>
                                                <li><a href="<?= base_url('services/ndt') ?>">Non-Destructive Testing</a></li>
                                                <li><a href="<?= base_url('services/corrosion-and-control') ?>">Corrosion Prevention and Control Services</a></li>
                                                <li><a href="<?= base_url('services/hydrotest') ?>">Hydro Test</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="<?= base_url('services/equipment') ?>">Equipment Leasing and Support</a></li>
                                        <li><a href="<?= base_url('services/engineering') ?>">Engineering and Technical Services</a></li>
                                        <li><a href="<?= base_url('services/procurement') ?>">Procurement</a></li>
                                        
                                    </ul>
                                </li>
                                <li class="<?= ($controller == 'contact') ? 'current' : ''; ?>"><a href="<?= base_url('contact') ?>">Contact</a></li>
                            </ul>