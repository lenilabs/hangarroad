<section class="page-banner" style="background-image:url(<?= base_url('assets/images/background/equipent_leasing.jpg') ?>);background-position:20% 75%;">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Equipment Leasing and Support </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
					<li><a href="javascript:;">Services</a></li>
                    <li>Equipment Leasing and Support </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Equipment Leasing and Support</h2>
								</div>
								<div class="bold-text">As an international Equipment leasing Company, we  provide the competitive rates and stability you need with Equipment Leasing. We have engineers who have been trained to install, operate and troubleshoot all the equipment we deal with. If you are a business owner who recognizes the advantages of Equipment Leasing, Hangar Road International is the partner you need in today's competitive world.</div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img src="<?= base_url('assets/images/services/leasing.jpg') ?>" alt="" />
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <p>We lease equipments such as.</p>
                                                <ul class="list-style-one">
                                                    <li>Badges</li>
                                                    <li>Bulldozers</li>
                                                    <li>Cranes</li>
                                                    <li>Dump Trucks</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Our Competitive Advantage</h3>
                                    <p>We operate in the area of Equipment Leasing, with the following advantages over our competitors.</p>
                                    <div class="services-featured-outer">
										<div class="row clearfix">
											
											<!-- Feature Block -->
											<div class="feature-block col-sm-12">
												<div class="inner-box">
													<div class="icon-box">
														<span class="icon flaticon-home-2"></span>
													</div>
													<h4>Wealth of experience</h4>
													<div class="featured-text">As an International Leasing firm with vast of experience, we can offer specialty advice and knowledge on required equipment depending on the nature of the job. Our loyalty and commitment to our clients is amongst our highest priorities. We build more than leases, We build relationships.</div>
												</div>
											</div>
											
											<!-- Feature Block -->
											<div class="feature-block col-sm-12">
												<div class="inner-box">
													<div class="icon-box">
														<span class="icon flaticon-fan"></span>
													</div>
													<h4>World Class Customer Service</h4>
													<div class="featured-text">we continually strive to maintain our standard by providing world-class customer service. You can trust that when working with Hangar Road International, we will work on establishing a relationship and partnership that will continue and grow as does your business.</div>
												</div>
											</div>
											
											<!-- Feature Block -->
											<div class="feature-block col-sm-12">
												<div class="inner-box">
													<div class="icon-box">
														<span class="icon flaticon-worker"></span>
													</div>
													<h4>All Expert Engineers</h4>
													<div class="featured-text">Our expert staffs have over 30 years industry experience in Equipment installation, operation and troubleshooting. You can rely on our professionals to provide you with equipments which will enable you accomplish all your task.</div>
												</div>
											</div>
										</div>
									</div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>