<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Hangar Road International Ltd</title>
<!-- Stylesheets -->
<link href="<?= base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
<link href="<?= base_url('assets/plugins/revolution/css/settings.css') ?>" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="<?= base_url('assets/plugins/revolution/css/layers.css') ?>" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="<?= base_url('assets/plugins/revolution/css/navigation.css') ?>" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->

<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
<link href="<?= base_url('assets/css/responsive.css') ?>" rel="stylesheet">

<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
<body>
<div class="page-wrapper">
    <!-- Preloader -->
    <!-- <div class="preloader"></div> -->

    <header class="main-header header-style-two">
        <!--Header Top-->
        <div class="header-top-two">
            <div class="auto-container clearfix">
                <div class="top-left clearfix">
                    <div class="text">We offer revolution of industrial engineering</div>
                </div>
                <div class="top-right clearfix">
                    <!-- Social Links -->
                    <ul class="social-links clearfix">
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                        <li><a href="#"><span class="fab fa-google-plus-g"></span></a></li>
                        <li><a href="#"><span class="fab fa-linkedin-in"></span></a></li>
                        <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                    </ul>

                    <!-- Change Location -->
					<div class="change-location">
                        <div class="location dropdown"><a class="btn btn-default dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Nigeria &nbsp;<span class="fa fa-angle-down"></span></a>
                            <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu2">
                                <li><a href="#">Nigeria</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->

        <!-- Header Upper -->
        <div class="header-upper">
            <div class="inner-container">
                <div class="auto-container clearfix">
                    <!--Logo-->
                    <div class="logo-outer">
                        <div class="logo"><a href="<?= base_url() ?>"><img src="<?= base_url('assets/images/hangarroad.png') ?>" alt="" width="160" style="margin-top:-15px;" title="hangar Road International"></a></div>
                    </div>

                    <!--Info-->
                    <div class="info-outer clearfix">
                        <!--Info Box-->
                        <div class="info-box">
                            <div class="icon"><span class="icon-call-in"></span></div>
                            <div class="text">
                                <strong>Call Us Today</strong>
                                <span class="info"><a href="#">+234 (01) 2772332</a></span>
                            </div>
                        </div>
                        <!--Info Box-->
                        <div class="info-box">
                            <div class="icon"><span class="icon-clock"></span></div>
                            <div class="text">
                                <strong>Working Hours</strong>
                                <span class="info">Mon - fri: 9:00am to 5:00pm</span>
                            </div>
                        </div>
                        <!--Info Box-->
                        <div class="info-box">
                            <div class="icon"><span class="icon-home"></span></div>
                            <div class="text">
                                <strong>2B Lawrence Omole Close</strong>
                                <span class="info">Off Sinari Daranijo VI, Lagos.</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <div class="header-lower">
            <div class="auto-container">
                <!--Nav Box-->
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="navbar-header">
                            <!-- Togg le Button -->      
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon flaticon-menu-button"></span>
                            </button>
                        </div>
                        
                        <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                            <?php $this->load->view('partials/navigation'); ?>
                        </div>
                    </nav>
                    <!-- Main Menu End-->

                    <!-- Main Menu End-->
                    <div class="outer-box clearfix">

                        <!-- Button Box -->
                        <div class="btn-box">
                            <a href="<?= base_url('contact') ?>" class="theme-btn btn-style-one">Send us a message</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="<?= base_url() ?>" title=""><img src="<?= base_url('assets/images/hangarroad.png') ?>" alt="hangarroad" width="120" title=""></a>
                </div>
                <!--Right Col-->
                <div class="pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-collapse show collapse clearfix">
                            <?php $this->load->view('partials/navigation'); ?>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div><!-- End Sticky Menu -->
    </header>

<?= $yield ?>

<!--Main Footer-->
<footer class="main-footer">
    
    <div class="auto-container">
    
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">
                
                <!--big column-->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">
                    
                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget about-widget">
                                <div class="logo">
                                    <img src="<?= base_url('assets/images/hangarroad.png') ?>" width="120" alt="hangarroad International" />
                                </div>
                                <div class="text">OUR business values set us apart from other firms in the business industry. We combine keen entrepreneurial instinct with rigorous approach to risk and responsibility. It is very safe to say that we are courageous with opportunity and conservative with risk    .</div>
                                <a href="<?= base_url('about') ?>" class="theme-btn btn-style-four">About Company</a>
                            </div>
                        </div>
                        
                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget services-widget">
                                <h2>Our Services</h2>
                                <ul class="footer-service-list">
                                    <li><a href="<?= base_url('services/environment') ?>">Environment Restoration Services</a></li>
                                    <li><a href="<?= base_url('services/facility') ?>">Facility Maintenance</a></li>
                                    <li><a href="<?= base_url('services/equipment') ?>">Equipment Leasing and <br/> Support</a></li>
                                    <li><a href="<?= base_url('services/engineering') ?>">Engineering and Technical <br/> Services</a></li>
                                    <li><a href="<?= base_url('services/procurement') ?>">Procurement</a></li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <!--big column-->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">
                    
                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <!-- <div class="footer-widget services-widget">
                                <h2>Our Industries</h2>
                                <ul class="footer-service-list">
                                    <li><a href="#">Oil & Gas</a></li>
                                    <li><a href="#">Construction</a></li>
                                    <li><a href="#">Marine</a></li>
                                    <li><a href="#">Energy</a></li>
                                </ul>
                            </div> -->
                            <div class="footer-widget services-widget">
                                <h2>Useful Links</h2>
                                <ul class="footer-service-list">
                                    <li><a href="#">Careers</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget contact-widget">
                                <h2>Contact</h2>
                                <div class="number">+234 (01) 2772332</div>
                                <ul>
                                    <li>2B Lawrence Omole Close Off Sinari Daranijo VI, Lagos.</li>
                                    <li><a href="#">info2@hangarroadinternational.com</a></li>
                                    <li>Mon to Fri: 9:00am to 5:00pm</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
        
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="clearfix">
                <div class="pull-left">
                    <div class="copyright">&copy; <?= date('Y') ?> Hangar Road International All rights reserved.</div>
                </div>
                <div class="pull-right">
                    <!-- Social Links -->
                    <ul class="social-links">
                        <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                        <li><a href="#"><span class="fab fa-google-plus-g"></span></a></li>
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-linkedin-in"></span></a></li>
                        <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        
    </div>
</footer>

<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>
<!--Scroll to top-->

<script src="<?= base_url('assets/js/jquery.js') ?>"></script>
<script src="<?= base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<!--Revolution Slider-->
<script src="<?= base_url('assets/plugins/revolution/js/jquery.themepunch.revolution.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/jquery.themepunch.tools.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.actions.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.carousel.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.migration.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.navigation.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.parallax.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/revolution/js/extensions/revolution.extension.video.min.js') ?>"></script>
<script src="<?= base_url('assets/js/main-slider-script.js') ?>"></script>
<!--Revolution Slider-->

<script src="<?= base_url('assets/js/jquery-ui.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.fancybox.js') ?>"></script>
<script src="<?= base_url('assets/js/owl.js') ?>"></script>
<script src="<?= base_url('assets/js/wow.js') ?>"></script>
<script src="<?= base_url('assets/js/appear.js') ?>"></script>
<script src="<?= base_url('assets/js/script.js') ?>"></script>
</body>
</html>