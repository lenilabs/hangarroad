<section class="main-slider-two">
        
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_two_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_two" data-version="5.4.1">
                <ul>
                    <li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?= base_url('assets/images/sliders/slide_1.jpg') ?>">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?= base_url('assets/images/sliders/slide_1.jpg') ?>">
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['650','650','650','450']"
                        data-whitespace="normal"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['-80','-110','-110','-130']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Build Superior Quality Industrial Solutions</h2>
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['650','650','650','450']"
                        data-whitespace="normal"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['35','10','10','10']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="text" style="font-size:15px;">We seek to provide our clients the best service by applying the core values of engineering, which is providing the most efficient solution to problems in a safe environment at minimal cost. Using the most innovative technology of the day.</div>
                        </div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="normal"
                        data-width="['650','650','650','450']"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['135','100','120','150']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="link-box clearfix">
                                <a href="<?= base_url('about') ?>" class="theme-btn btn-style-two">Learn more</a>
                                <!-- <a href="<?//= base_url('services') ?>" class="theme-btn btn-style-six">Our Services</a> -->
                            </div>
                        </div>
					
                    </li>
                    
                    <li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?= base_url('assets/images/sliders/slide_2.jpg') ?>">
                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?= base_url('assets/images/sliders/slide_2.jpg') ?>">
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['650','650','650','450']"
                        data-whitespace="normal"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['-80','-110','-110','-110']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Build Superior Quality Industrial Solutions</h2>
                        </div>
                    
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['650','650','650','450']"
                        data-whitespace="normal"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['35','10','10','10']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="text" style="font-size:15px;">We seek to provide our clients the best service by applying the core values of engineering, which is providing the most efficient solution to problems in a safe environment at minimal cost. Using the most innovative technology of the day.</div>
                        </div>
                    
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="normal"
                        data-width="['650','650','650','450']"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['135','100','120','120']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="link-box clearfix">
                                <a href="<?= base_url('about') ?>" class="theme-btn btn-style-two">Learn more</a>
                                <!-- <a href="<?//= base_url('services') ?>" class="theme-btn btn-style-six">Our Services</a> -->
                            </div>
                        </div>
                    
                    </li>
                    
                </ul>
            </div>
        </div>
    </section>

    <!-- Services Section Three -->
	<section class="services-section-three">
		<div class="auto-container">
			<div class="row clearfix">
					
                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <div class="title">Hangar Road International Limited</div>
                            <h2>What We Do</h2>
                        </div>
                        <div class="text">
                            <p>We have a wide range of services to maximize the performance of any completion project.  We've pushed the industry forward with new technologies focused on one goal; solving your challenges in unconventional areas, while establishing a relationship and partnership that will continue and grow as your business.</p>
                            <p>Here's a list of some of our services.</p>
                        </div>
                        <div class="link-box"></div>
                    </div>
                </div>
                
                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="<?= base_url('services/engineering') ?>"><img src="<?= base_url('assets/images/services/construction.png') ?>" style="height:250px" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="<?= base_url('services/engineering') ?>">Engineering & Technical Support</a></h3>
                            <div class="text">Our unique skilled personnel allow us to provide a wide range of engineering and technical support for the oil and gas industry. Our engineering and technical support services covers both offshore and onshore.</div>
                            <a href="<?= base_url('services/engineering') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="<?= base_url('services/equipment') ?>" ><img src="<?= base_url('assets/images/services/leasing.jpg') ?>" style="height:250px" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="<?= base_url('services/equipment') ?>" >Equipment leasing and Support</a></h3>
                            <div class="text">As an international Equipment leasing Company , we  provide the competitive rates and stability you need with Equipment Leasing. </div>
                            <a href="<?= base_url('services/equipment') ?>"  class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="<?= base_url('services/pipeline') ?>"><img src="<?= base_url('assets/images/services/pipes.jpg') ?>" style="height:250px" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="<?= base_url('services/pipeline') ?>">Pipe Maintenance Services</a></h3>
                            <div class="text">HRI has the technology and equipment available to deal with all pipe dimensions. We are into pipe cleaning, surface treatment and inspection / testing. Our pipeline services cover both offshore and onshore.</div>
                            <a href="<?= base_url('services/pipeline') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="<?= base_url('services/environment') ?>"><img src="<?= base_url('assets/images/services/clean_up.jpg') ?>" style="height:250px" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="<?= base_url('services/environment') ?>">Environmental Restoration Services</a></h3>
                            <div class="text">HRI has its treatment facility for carrying out waste management service. We provide a range of waste management and environmental restoration services with our high level skilled personnel in environmental management.</div>
                            <a href="<?= base_url('services/environment') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="<?= base_url('services/procurement') ?>"><img src="<?= base_url('assets/images/services/procure.jpg') ?>" style="height:250px" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="<?= base_url('services/procurement') ?>">Procurement Services</a></h3>
                            <div class="text">Hangar Road operates a procurement department that is involved in direct procurement and sales for customers. We have partnered with some of the world’s leading manufacturers of oil and gas tools and equipments. These companies manufacture quality tools and drilling equipment i.e. drill pipes, drill collars, tubing, shakers, pumps, gauges, unions e.t.c.</div>
                            <a href="<?= base_url('services/procurement') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</section>
    <!-- End Services Section Three -->
    
    <section class="services-section-two">
        <div class="auto-container">
            <div class="inner-container">
                <div class="big-icon flaticon-settings-4"></div>
                <!-- Sec Title -->
                <div class="sec-title light">
                    <div class="row clearfix">
                        <div class="pull-left col-xl-4 col-lg-5 col-md-12 col-sm-12">
                            <div class="title">Hangar Road International</div>
                            <h2> <br> Our Industries</h2>
                        </div>
                        <div class="pull-left col-xl-8 col-lg-7 col-md-12 col-sm-12">
                            <div class="text">We continually seek to provide the best service by applying the core values of engineering to a wide range of industries, some of them include.</div>
                        </div>
                    </div>
                </div>
            
                <div class="three-item-carousel owl-carousel owl-theme">
                    
                    <!-- Services Block Two -->
                    <div class="services-block-two">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?= base_url('assets/images/services/oil_and_gas.jpg') ?>" style="height:220px;" alt="" />
                            </div>
                            <div class="lower-content">
                                <h3 class="text-white">Oil & Gas Engineering</h3>
                                <!-- <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div> -->
                                <!-- <a href="javascript:;" class="read-more">Read More <span class="arrow flaticon-next"></span></a> -->
                            </div>
                        </div>
                    </div>

                    <div class="services-block-two">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?= base_url('assets/images/construction.jpg') ?>" style="height:220px;" alt="" />
                            </div>
                            <div class="lower-content">
                                <h3 class="text-white">Construction</h3>
                                <!-- <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div> -->
                                <!-- <a href="#" class="read-more">Read More <span class="arrow flaticon-next"></span></a> -->
                            </div>
                        </div>
                    </div>
                    
                    <!-- Services Block Two -->
                    <div class="services-block-two">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?= base_url('assets/images/energy.jpg') ?>" style="height:220px;" alt="" />
                            </div>
                            <div class="lower-content">
                                <h3 class="text-white">Energy</h3>
                                <!-- <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div> -->
                                <!-- <a href="#" class="read-more">Read More <span class="arrow flaticon-next"></span></a> -->
                            </div>
                        </div>
                    </div>
                    
                    <!-- Services Block Two -->
                    
                    
                    <!-- Services Block Two -->
                    <div class="services-block-two">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?= base_url('assets/images/services/marine.png') ?>" style="height:220px;"  alt="" />
                            </div>
                            <div class="lower-content">
                                <h3 class="text-white">Marine</h3>
                                <!-- <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div> -->
                                <!-- <a href="javascript:;" class="read-more">Read More <span class="arrow flaticon-next"></span></a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="approach-section" style="background-image: url();   ">
        <div class="auto-container">
            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <div class="icon"><img src="images/icons/logo-icon.png" alt=""></div>
                            <div class="title">We are Hanger Road Intenational</div>
                            <h2>Come Do Business With Us</h2>
                            <div class="text mb-4">Sustainability is integral to our overall strategy as we seek to deliver long-term financial value while minimizing our environmental footprint and having a positive impact on society. This is expressed in our Guiding Principles for Sustainability.</div>
                            <hr/>
                            <h5 style="font-weight:bolder;color:#000;" class="text-bold">Ethics and Integrity are the foundation for our Guiding Principles</h5>
                            <p class="text" style="margin-top:10px;">Built on a solid foundation of ethics and integrity, the Hangar Road International Limited Guiding Principles for Sustainability provide the framework for our operations and our future. To ensure that these principles guide every aspect of our decisions, plans and actions, we have matched each with a clearly defined intent.</p>
                        </div>

                        <div class="">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:50%">Principle</th>
                                        <th>Intent</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 class="text hangarroad-green-text">Financial Performance</h4>
                                            <p>Deliver superior value for our customers</p>
                                        </td>
                                        <td>
                                            To outperform our competitors by delivering superior growth, margins and returns to our customers
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h4 class="text hangarroad-green-text">Health, Safety & Environment</h4>
                                            <p>Conduct operations that are safe and environmentally responsible</p>
                                        </td>
                                        <td>
                                            To advance on our Journey to ZERO, toward our vision of zero health, safety, environment or service quality incidents.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h4 class="text hangarroad-green-text">Technology & Innovation</h4>
                                            <p>Lead the industry in innovation and conscientious stewardship of global resources</p>
                                        </td>
                                        <td>
                                            To develop solutions that give our customers economic access to new hydrocarbon resources and maximize the value of their existing assets
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h4 class="text hangarroad-green-text">Global Citizenship</h4>
                                            <p>Enhance the economic and social well-being of our employees and the communities in which we operate</p>
                                        </td>
                                        <td>
                                            To be preferred employer and make a positive impact in the communities where we live and work
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h4 class="text hangarroad-green-text">Transparency</h4>
                                            <p>Be transparent in reporting and validating our progress</p>
                                        </td>
                                        <td>
                                            To provide our stakeholders with thorough and timely information on our progress
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h4 class="text hangarroad-green-text">Collaboration</h4>
                                            <p>Engage our stakeholders to help achieve results that are compatible with our stated principles</p>
                                        </td>
                                        <td>
                                            To actively communicate with key stakeholders to help achieve mutual objectives
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12 mt-5">
                    <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image"><a href="<?= base_url('assets/images/business.jpeg') ?>" class="lightbox-image"><img src="<?= base_url('assets/images/business.jpeg') ?>" alt=""></a></figure>
                        <div class="mt-5">
                            <h4 style="color:#000" class="">RENEWABLE ENERGY SOLUTION</h4>
                            <p class="text mb-4 mt-3">
                                Many of our projects in Nigeria benefit from a combination of different energy technologies and HRI has extensive knowledge in working with customers to define the specific needs of the project and the best integrated solutions which cut energy costs and deliver security of supply and reliability. Much more than an oil and gas servicing company; HRI is a highly experienced indigenous company that can take care of everything for long term investment.
                            </p>
                            <p>
                                With over 10 years experience and as a force in Nigeria in renewable energy solutions, developing reliable, adaptable and affordable solutions that cut energy costs is at the heart of everything we do.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="quote-section wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
        <div class="auto-container">
            <div class="inner-section">
                <div class="clearfix">
                    
                    <div class="pull-left">
                        <div class="content">
                            <div class="icon flaticon-hammer"></div>
                            <h3>Have a project you would like to discuss?</h3>
                            <div class="text">We always bring good quality services with 100% safety measures</div>
                        </div>
                    </div>
                    
                    <div class="pull-right">
                        <a href="<?= base_url('contact') ?>" class="theme-btn btn-style-three">Send us a message</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>