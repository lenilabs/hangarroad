<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/facility-aerial.jpg') ?>);background-position:25% 20%;">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Facility Maintenance </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
					<li><a href="javascript:;">Services</a></li>
                    <li>Facility Maintenance </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
                        <div class="row">
                            <!-- Services Block Three -->
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/pipeline') ?>"><img src="<?= base_url('assets/images/services/piping.jpg') ?>" style="height:250px" alt="" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/pipeline') ?>">Pipeline Maintenance</a></h3>
                                        <a href="<?= base_url('services/pipeline') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/pipeline-pigging') ?>"><img src="<?= base_url('assets/images/services/pigging-pipes.jpeg') ?>" style="height:250px" alt="" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/pipeline-pigging') ?>">Pipeline Pigging and Inspections</a></h3>
                                        <a href="<?= base_url('services/pipeline-pigging') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/ndt') ?>"><img src="<?= base_url('assets/images/services/ndt-ultrasonic.jpg') ?>" style="height:250px" alt="" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/ndt') ?>">Non Destructive Testing (NDT)</a></h3>
                                        <a href="<?= base_url('services/ndt') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/corrosion-and-control') ?>"><img src="<?= base_url('assets/images/services/pipe_painting.jpg') ?>" style="height:250px" /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/corrosion-and-control') ?>">Corrosion Prevention and Control Services</a></h3>
                                        <a href="<?= base_url('services/corrosion-and-control') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>        
                            <div class="services-block-three col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="image">
                                        <a href="<?= base_url('services/hydrotest') ?>"><img src="<?= base_url('assets/images/services/hydrotest.jpg') ?>" style="height:250px"  /></a>
                                    </div>
                                    <div class="lower-content">
                                        <h3><a href="<?= base_url('services/hydrotest') ?>">Hydro Test</a></h3>
                                        <a href="<?= base_url('services/hydrotest') ?>" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>                
                        </div>
					</div>
				</div>
				
			</div>
		</div>
	</div>