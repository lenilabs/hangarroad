<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/oil-spill-response-equipment.jpg') ?>);background-position:25% 40%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1 style="font-size:45px;">Oil Spill Response Equipment and Material Supplies </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/environment') ?>">Enviromental Restoration Services</a></li>
                    <li>Oil Spill Response Equipment and Material Supplies</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Oil Spill Response Equipment and Material Supplies</h2>
                                </div>

								<div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>How prepared are we to handle a spill?</h3>
                                                    <p>An effective response depends on competent personnel working to a well developed plan that has been adequately resourced and regularly exercised. Our Preparedness Services will ensure that plans are properly developed. We have the right equipment. </p>
                                                    <p>We offer an end-to-end oil spill service that provides an integrated solution to all your oil spill preparedness and response needs. Through open communication with our Members, regulators and other stakeholders we are able to understand the needs and desires of everyone involved. Our knowledge combined with experience and expertise allows us to create customised, targeted and cost effective oil spill response solutions that support our customers activities.</p>
                                                    <p>Wherever your oil spill risks lie in the supply chain, we are ready to respond with our expertise and resources anytime, anywhere.</p>
                                                    <p>We have a diverse portfolio of people who have the practical skills and experience to assist and support our Members during a spill response and are trained in using the Incident Command System (ICS) structure.</p>
                                                    <p>We continue to improve the capability and capacity of our response personnel and equipment, setting a foundation for coordinated response during major spill incidents.</p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>