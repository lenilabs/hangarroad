<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/pipeline-pigging.jpg') ?>);background-position:25% 60%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Pipeline Pigging and Inspection </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/facility') ?>">Facility Maintenance</a></li>
                    <li>Pipeline Pigging and Inspection </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Pipeline Pigging and Inspection</h2>
                                </div>
                                <div class="bold-text">Pigging is an in-line inspection (ILI) technique in which devices referred to “pigs” are inserted into pipelines to perform cleaning and inspection activities. Pigging can be conducted on a variety of pipelines sizes without having to stop the flow of material through the line.</div>

								<div class="bold-text">In-line inspections scan for issues that may present evidence of general pitting or other corrosion forms such as:</div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img src="<?= base_url('assets/images/services/pigging-pipes.jpeg') ?>" style="height:250px;"/>
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <ul class="list-style-one">
                                                    <li>Metal loss</li>
                                                    <li>Cracks (SCC) </li>
                                                    <li>Dents</li>
                                                    <li>Gouges</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <p>Our technique involves a pig being placed into the pipeline at a valve or pump station that has a special configuration of valves and pipes where the tool can be loaded into a receiver. Once the receiver is closed and sealed, the pig is then driven down the line, either being pulled through by a cable or being pushed through by the flow of product.
                                        <br/>
                                        Traditionally, pigging was used purely as a way to clean pipelines. The pig was simply placed in one end of a pipeline and pushed through the line by the product flow. It scrapes the sides of the pipe as it travels, removing dirt and debris as it goes.
                                    </p>

                                    <p>For inspection, pigs can be fitted with various non-destructive examination technologies that can scan the pipe through which it travels. These are often referred to as "smart pigs." There are several different types of smart pigs utilized in ILI activities, each with its own set of advantages and disadvantages. Some are more effective at detecting certain types of corrosion or damage in different types of pipes, depending on their NDE capabilities. We combine the various functions of these separate tools into one. This way a single tool can now be used to detect several different types of damage, making it more efficient and effective.</p>

                                    <p>Inspection methods include magnetic flux leakage (MFL) and ultrasonics (UT). MFL is a method where a strong magnetic flux is induced into the pipeline wall. Sensors then pick up any leakage of this flux and the extent of the leakage indicates a flaw in the pipe wall. Material loss from the pipe wall, for example, will cause flux leakage that should be identified by the pig’s sensors. All defects are then logged.</p>

                                    <p>Ultrasonic inspection is a direct measurement of the wall thickness and can identify cracks in the pipe wall. A transducer emits a pulse of ultrasonic sound that travels at a specific speed. The time taken for the echo to return to the sensor is a measurement of the thickness of the pipe wall. This technique requires a liquid medium for the pulse to travel.</p>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>