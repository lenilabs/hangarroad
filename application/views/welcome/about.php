 <!--Page Title-->
 <section class="page-banner style-two" style="background-image:url(<?= base_url('assets/images/background/about.jpg') ?>);background-position:25% 20%;">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>About Us</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li>About Hangar Road International</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Fun Facts Section -->
    <section class="about-section-two">
        <div class="auto-container">
            <div class="title-style-one style-two centered">
                <!-- <div class="icon"><img src="images/icons/logo-icon.png" alt=""></div> -->
                <div class="subtitle">We are Hangar Road International</div>
                <h2>We take pride in delivering value for our customers</h2>
                <div class="text">At Hangar Road, we seek to provide our clients the best service by applying the core values of engineering, which is providing the most efficient solution to problems in a safe environment at minimal cost, using the most innovative technology of the day.</div>
            </div>

            <div class="row">
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <figure class="image"><a href="<?= base_url('assets/images/sliders/engineers_long.jpg') ?>" class="lightbox-image"><img src="<?= base_url('assets/images/sliders/engineers_long.jpg') ?>" alt=""></a></figure> 
                    </div>
                </div>

                <div class="content-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="text test-justify">
                            <p><strong>Hangar Road International Limited is an oil and gas servicing company incorporated in 2008 by the CAMA 1990 as a limited liability company, fully 100% Nigeria owned company.</strong></p>
                            <p>We offer excellent support services on waste management, soil remediation, procurement; heavy duty equipment leasing and equipment supply, pipeline construction, operation and maintenance services, engineering design and other related services.</p>
                            <p>Hangar Road is organised to effectively manage and execute all the services, which are undertaken under contract, and is registered as contractor with the major oil and gas companies.</p>
                        </div>
                        <div class="fact-counter wow fadeInRightBig" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="clearfix">
                                <!--Column-->
                                <div class="column counter-column col-lg-4 col-md-4 col-sm-12">
                                    <div class="inner">
                                        <div class="count-outer count-box">
                                            <span class="count-text" data-speed="2000" data-stop="10">0</span>+
                                            <h4 class="counter-title">Years Experience</h4>
                                        </div>
                                    </div>
                                </div>
                                
                                <!--Column-->
                                <div class="column counter-column col-lg-4 col-md-4 col-sm-12">
                                    <div class="inner">
                                        <div class="count-outer count-box">
                                            <span class="count-text" data-speed="2500" data-stop="8">0</span>
                                            <h4 class="counter-title">Industries Served</h4>
                                        </div>
                                    </div>
                                </div>
                                
                                <!--Column-->
                                <div class="column counter-column col-lg-4 col-md-4 col-sm-12">
                                    <div class="inner">
                                        <div class="count-outer count-box">
                                            <span class="count-text" data-speed="3500" data-stop="60">0</span>
                                            <h4 class="counter-title">  Available Services</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Fun Facts Section -->

    <section class="mission-section" style="background-image: url(images/background/mission-bg.jpg);">
        <div class="auto-container text-white">
            <!-- Feature Block Seven -->
            <div class="feature-block-seven">
                <div class="row clearfix">
                    <div class="image-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <figure class="mt-5 image-box"><a href="<?= base_url('assets/images/mission.jpg') ?>" class="lightbox-image"><img src="<?= base_url('assets/images/mission.jpg') ?>" alt="Hangar Road Mission"></a></figure>
                        </div>
                    </div>
                    <div class="content-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <!-- Sec Title -->
                            <div class="sec-title">
                                <h2 class="text-white">Our Misson</h2>
                            </div>
                            <div class="text-white text">To be the first company of choice in our core area of business to our clients and to add unquestionable value by our "quality on time" approach to the sector.</div>
                            <div class="text-white text">we seek to provide our clients with the best service by applying the core values of engineering, which is providing the most efficient solution to problems in a safe environment at minimal cost, using the most innovative technology of the day.    
                        </div>
                    </div>
                </div>
            </div>

            <!-- Feature Block Seven -->
            <div class="feature-block-seven style-two">
                <div class="row clearfix">
                    <div class="image-column col-lg-6 col-md-12 col-sm-12 order-2">
                        <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <figure class="image-box"><a href="<?= base_url('assets/images/vision.jpg') ?>" class="lightbox-image"><img src="<?= base_url('assets/images/vision.jpg') ?>" alt=""></a></figure>
                        </div>
                    </div>
                    <div class="content-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <!-- Sec Title -->
                            <div class="sec-title">
                                <h2 class="text-white">Our Vision</h2>
                            </div>
                            <div class="text text-white">Hangar Road’s vision is to be the most reputable, environmentally friendly, profitable and most recognized employer of local content through out West Africa while never compromising on "our quality on time" approach. 
                                <br/>This approach to business and the market has led to the continued and sustained growth of Hangar Road International, resulting in the creation of employment and constant development at all levels of our business.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>