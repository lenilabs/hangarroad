<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                	<aside class="sidebar padding-right">
						
						<!-- Category Widget -->
                        <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <ul class="services-categories">
                                    <?php $method = strtolower($this->uri->segment(2)) ?>
                                    <li class="<?= ($method == "oil_and_gas") ? 'active' : '' ?>"><a href="<?= base_url('industries/oil_and_gas') ?>">Oil & Gas</a></li>
                                    <li class="<?= ($method == "marine") ? 'active' : '' ?>"><a href="<?= base_url('industries/marine') ?>">Marine</a></li>
                                    <li class="<?= ($method == "construction") ? 'active' : '' ?>"><a href="<?= base_url('industries/construction') ?>">Construction</a></li>
                                    <li class="<?= ($method == "energy") ? 'active' : '' ?>"><a href="<?= base_url('industries/energy') ?>">Energy</a></li>
                                </ul>
                            </div>
                        </div>
						
						<!-- Brochures Widget -->
                        <div class="sidebar-widget brochures">
                            <div class="sidebar-title"><h5>Service Downloads</h5></div>
                            <div class="widget-content">
                                <!-- <a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Presentation PDF</a> -->
                                <a href="#" class="brochure-btn"><span class="icon fas fa-file-alt"></span> Brochure DOC</a>
                            </div>
						</div>
						
						<!-- Services Widget -->
                        <div class="sidebar-widget services-widget">
							<div class="widget-content" style="background-image:url(<?= base_url('assets/images/support.jpg') ?>)">
								<div class="icon flaticon-settings-4"></div>
								<h3>Optimising Perfomance with Special Services</h3>
								<div class="text">Discover how we're improving <br> quality of Industries</div>
								<a href="#" class="theme-btn btn-style-two">get in touch</a>
							</div>
						</div>
						
						<!-- Support Widget -->
                        <div class="sidebar-widget support-widget">
                            <div class="widget-content">
                                <span class="icon flaticon-telephone-1"></span>
                                <div class="text">Got any Questions? <br> Call us Today!</div>
                                <div class="number">+234 (01) 2772332</div>
                                <div class="email"><a href="#">info2@hangarroadinternational.com</a></div>
                            </div>
                        </div>
						
					</aside>
                </div>