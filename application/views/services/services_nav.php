<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                	<aside class="sidebar padding-right">
						
						<!-- Category Widget -->
                        <div class="sidebar-widget categories-two">
                            <div class="widget-content">
                                <!-- Services Category -->
                                <ul class="services-categories">
                                    <?php $method = strtolower($this->uri->segment(2)) ?>
                                    <?php $environmental_array = ["environment", "oilspill-cleanup-and-recovery",  "remediation-and-rehabilitation", "oilspill-response-equipment", "oil-waste-handling"]; ?>
                                    <?php $facility_array = ["facility", "pipeline",  "pipeline-pigging", "ndt", "corrosion-and-control", "hydrotest"]; ?>
                                    <!-- <li class="<?//= ($method == "oil_and_gas") ? 'active' : '' ?>"><a href="<?= base_url('services/oil_and_gas') ?>">Oil & Gas</a></li> -->
                                    <li class="<?= (in_array($method, $environmental_array)) ? 'active' : '' ?>"><a href="<?= base_url('services/environment') ?>">Environment Restoration Services</a></li>
                                    <li class="<?= (in_array($method, $facility_array)) ? 'active' : '' ?>"><a href="<?= base_url('services/facility') ?>">Facility Maintenance</a></li>
                                    <li class="<?= ($method == "equipment") ? 'active' : '' ?>"><a href="<?= base_url('services/equipment') ?>">Equipment Leasing and Support</a></li>
                                    <li class="<?= ($method == "engineering") ? 'active' : '' ?>"><a href="<?= base_url('services/engineering') ?>">Engineering and <br/>Technical Services</a></li>
                                    <li class="<?= ($method == "procurement") ? 'active' : '' ?>"><a href="<?= base_url('services/procurement') ?>">Procurement</a></li>
                                    
                                </ul>
                            </div>
                        </div>
						
						<!-- Brochures Widget -->
                        <div class="sidebar-widget brochures">
                            <div class="sidebar-title"><h5>Service Downloads</h5></div>
                            <div class="widget-content">
                                <!-- <a href="#" class="brochure-btn"><span class="icon fas fa-file-pdf"></span> Presentation PDF</a> -->
                                <a href="#" class="brochure-btn"><span class="icon fas fa-file-alt"></span> Brochure DOC</a>
                            </div>
						</div>
						
						<!-- Services Widget -->
                        <div class="sidebar-widget services-widget">
							<div class="widget-content" style="background-image:url(<?= base_url('assets/images/support.jpg') ?>)">
								<div class="icon flaticon-settings-4"></div>
								<h3>Optimising Perfomance with Special Services</h3>
								<div class="text">Discover how we're improving <br> quality of Industries</div>
								<a href="#" class="theme-btn btn-style-two">get in touch</a>
							</div>
						</div>
						
						<!-- Support Widget -->
                        <div class="sidebar-widget support-widget">
                            <div class="widget-content">
                                <span class="icon flaticon-telephone-1"></span>
                                <div class="text">Got any Questions? <br> Call us Today!</div>
                                <div class="number">+234 (01) 2772332</div>
                                <div class="email"><a href="#">info2@hangarroadinternational.com</a></div>
                            </div>
                        </div>
						
					</aside>
                </div>