<section class="page-banner" style="background-image:url(<?= base_url('assets/images/services/oil-spill-response.jpg') ?>);background-position:25% 60%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1 style="font-size:45px;">Oily Waste Handling And Management From Collection To Disposal </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/environment') ?>">Enviromental Restoration Services</a></li>
                    <li>Oily Waste Handling And Management From Collection To Disposal</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Oily Waste Handling And Management From Collection To Disposal</h2>
                                </div>

								<div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Disposal</h3>
                                                    <p>Most oil spills generate a lot of waste. This includes not just oil, but oiled cargo, debris, shoreline sediment, fauna and flora, equipment and protective clothing (PPE). Waste can sometimes amount to as much as ten times the volume of oil originally spilled. </p>
                                                    <p>As a consequence, waste can cause major logistical problems and delays for the clean-up operation and even bring the response to a standstill unless adequate arrangements are in place to deal with it.</p>
                                                </div>
                                                <div class="">
                                                    <h3>Planning</h3>
                                                    <p>Options for handling waste need to be considered even before an incident occurs, ie when preparing oil spill contingency plans. This means that in the event of a spill, decisions can be made quickly on appropriate and available methods for treating or disposing of the waste, as well as options for its storage and transport. Plans should be updated regularly to include any changes in local regulations.</p>
                                                </div>
                                            </div>
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <h3>Waste Management Options</h3>
                                            </div>  
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img src="<?= base_url('assets/images/services/oily-waste.png') ?>" />
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="">
                                                    <p>The overall approach when dealing with waste from an oil spill is the same as with any type of waste, i.e reduce, reuse, recycle by</p>
                                                    <ul class="list-style-one">
                                                        <li>Minimising the amount of oily waste collected by using selective clean-up techniques, where suitable, so that clean material is not picked up along with oiled. Close supervision of the workforce helps in this respect.</li>
                                                        <li>Washing and re-using equipment and resources, where possible.</li>
                                                        <li>Reprocessing oil through a refinery or recycling plant.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <p>In many cases, waste cannot be recycled or reprocessed and it ends up in landfill sites or going for incineration.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Amount & Type of Waste</h3>
                                                    <p>At the start of an incident, it helps to have a realistic estimate of how much waste there's likely to be and what it comprises.</p>
                                                    <p>As a general rule, spills of crude oils and heavy fuel oils persist in the marine environment and generate the most waste. Non-persistent oils, such as gasoline and kerosene, on the other hand, usually evaporate within a few hours of being spilled. As a result, the waste generated from spills of these products tends to be much less.</p>
                                                    <p>Over time, oil at sea may pick up water to form an emulsion. It may also collect debris, such as bits of lost cargo, broken parts of the ship or litter and other debris from the shore, adding to the volume to be recovered.</p>
                                                    <p>Oil that's recovered from the shore is usually mixed in with large quantities of sand or pebbles, and often wood, plastics, seaweed or oiled wildlife.</p>   
                                                    <p>The clean-up itself also generates waste, with oiled response materials, such as sorbents, PPE, damaged boom and waste bags adding to the volume to be removed. If fishing gear and mariculture facilities are contaminated and can't be cleaned satisfactorily, that also adds to the waste pile.</p>
                                                    <p>Different types of waste are often disposed of differently, so it helps to separate out the waste and keep it segregated from the outset. This isn't always easy, but it saves time in the long run and makes it much easier to direct the waste towards the appropriate treatment or disposal method.</p>
                                                </div>
                                                <div class="">
                                                    <h3>Waste Storage</h3>
                                                    <p>The waste collected during clean-up operations often needs to be stored temporarily to allow time for the logistics to be put in place to transport and dispose of it. Shoreline waste is normally stored in skips or drums well above the high tide mark. The storage area needs to be lined to make sure there's no contamination of the surrounding area or groundwater. It should also be separated from public access areas where possible.</p>
                                                    <p>Plastic bags and sacks are often used for collected waste, but as they deteriorate in sunlight they should only be used to transport waste rather than store it for any length of time.</p>
                                                    <p>During a large spill response, the amount of material collected may be more than the local treatment or disposal sites can cope with. In these cases, larger temporary storage facilities need to be found.</p>
                                                    <p>Waste needs to be handled in accordance with local regulations. In some countries, licences are required for temporary disposal sites and by the contractors used in various disposal tasks.</p>
                                                </div>
                                            </div>
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <h3>Treatment and Disposal Options</h3>
                                            </div>  
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img src="<?= base_url('assets/images/services/oily-waste.jpeg') ?>" />
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="">
                                                    <p>There are a number of options available for final treatment and disposal of oil and oily waste. The method taken depends on several factors:</p>
                                                    <ul class="list-style-one">
                                                        <li>The nature and consistency of the waste</li>
                                                        <li>The volume of waste collected</li>
                                                        <li>The availability of suitable sites and facilities.</li>
                                                        <li>The costs involved</li>
                                                        <li>Any regulatory restrictions</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-12 col-md-12 col-sm-12">
                                                <div class="">
                                                    <h3>Re-Use and Recycle</h3>
                                                    <p>The best and most obvious option is to recover as much of the actual oil as possible for reuse. Oil can be blended into feedstock for use in oil refineries or with fuel oils for burning in power stations. At the same time as reducing the amount of waste, this may also generate income which can offset the costs of disposal. It's an option that works best with oil collected from the sea rather than the shoreline as it typically contains less debris.</p>
                                                    <p>Another example is oily sand that doesn't contain too much debris. This can be stabilised using quicklime for use in land reclamation and road construction.</p>
                                                </div>
                                                <div class="">
                                                    <h3>Other Disposal Options</h3>
                                                    <p>In many cases, the most practical and cost-effective disposal options still take precedence over more sustainable waste management choices. This means that much oily waste goes for incineration or into landfill.</p>
                                                    <p>As a general rule, incinerators used for domestic waste are not suitable for disposal of large amounts of oil. Co-disposal of small amounts of oily waste with other refuse may be allowed at some facilities and oiled PPE, sorbents, netting etc that may have a low oil content are often treated in this way.</p>
                                                    <p>High temperature industrial waste incinerators are sometimes used but they don't always have sufficient spare capacity to deal rapidly with the additional burden created by the large quantities of waste from an oil spill.</p>
                                                    <p>In many cases the only realistic option for dealing with oil spill waste is landfill. Landfill sites have been used for the disposal of waste from spills for a long time, but, in many countries, they are now subject to tighter legislation. Sites are often licensed under specific conditions and may limit the volume and type of waste they will accept. In some parts of the world, oily waste will need to be disposed of in a site specifically designated for hazardous refuse.</p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>