<section class="page-banner" style="background-image:url(<?= base_url('assets/images/background/procurement.jpg') ?>">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Procurement </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
					<li><a href="javascript:;">Services</a></li>
                    <li>Procurement </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Procurement</h2>
								</div>
								<div class="bold-text">Hangar Road operates a procurement department that deals in the direct procurement and sales of equipment to customers. We have partnered with some of the world’s leading manufacturers of oil and gas tools and equipments. These companies manufacture quality tools and drilling equipment i.e. drill pipes, drill collars, tubing, shakers, pumps, gauges, unions e.t.c.</div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <div class="image">
                                                    <img  class="mb-2" src="<?= base_url('assets/images/services/valves_wide.jpg') ?>" alt="" />
                                                    <img src="<?= base_url('assets/images/services/procure.jpg') ?>" alt="" />
                                                </div>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <p>Our procurement is based on creative and cost effective control, we partake in the general engineering procurement of equipment for both onshore and offshore locations.</p>
                                                <ul class="list-style-one">
                                                    <li>Safety /Protection /Security/Fire fighting equipment</li>
                                                    <li>Material and Product Handling Equipment</li>
                                                    <li>Pumps and accessories</li>
                                                    <li>Tank and columns</li>
                                                    <li>Electrical equipment and materials</li>
                                                    <li>Cooling, heating and ventilation equipment</li>
                                                    <li>Marine/diving and pipeline</li>
                                                    <li>Valves/pipe/hoses and accessories as well as</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>