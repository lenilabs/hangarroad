<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends MY_Controller {
	public function index(){}
    
    public function oil_and_gas(){}

    public function environment(){}

    public function equipment(){}

    public function engineering(){}

    public function procurement(){}

    public function facility(){}

    public function pipeline(){}

    public function pigging(){}
    
    public function ndt(){}

    public function corrosion() {}

    public function hydrotest() {}

    public function oilspill() {}

    public function oilspill_equipment() {}

    public function oilwaste() {}

    public function remediation() {}
    
}
