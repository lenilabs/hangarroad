<section class="page-banner" style="background-image:url(<?= base_url('assets/images/sliders/slide_1.jpg') ?>">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Oil & Gas </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
					<li><a href="<?= base_url('services') ?>">Services</a></li>
                    <li>Oil & Gas </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							<div class="image">
								<img src="images/resource/service-12.jpg" alt="" />
							</div>
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Oil & Gas</h2>
								</div>
								<div class="bold-text">Consectetur adipisicing elit sed do eiusmod tempor dolor magna aliquat enim veniam, quis nostrud exdra anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan catium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</div>
								<div class="text">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea comds do consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla paria tur. Excepteur sint occaecat cupidatat non proident sunt in culpa.</p>
									<h3>Why Choose us</h3>
									<p>Auis nostrud exercitation ullamc laboris sed nisit aliquip ex bea sed consequat ipsum duis sit amet consecter adipisicing elit sed ipsum eiusmod tempor incididunt ut labore.</p>
									<!-- Services Featured Outer -->
									<div class="services-featured-outer">
										<div class="row clearfix">
											
											<!-- Feature Block -->
											<div class="feature-block col-lg-6 col-md-6 col-sm-12">
												<div class="inner-box">
													<div class="icon-box">
														<span class="icon flaticon-home-2"></span>
													</div>
													<h4>Well Maintained</h4>
													<div class="featured-text">Incididunt laboret dolore magna exercitation laboris nisis dolor in derit in voluptate velit.</div>
												</div>
											</div>
											
											<!-- Feature Block -->
											<div class="feature-block col-lg-6 col-md-6 col-sm-12">
												<div class="inner-box">
													<div class="icon-box">
														<span class="icon flaticon-fan"></span>
													</div>
													<h4>Modern Equipments</h4>
													<div class="featured-text">Incididunt laboret dolore magna exercitation laboris nisis dolor in derit in voluptate velit.</div>
												</div>
											</div>
											
											<!-- Feature Block -->
											<div class="feature-block col-lg-6 col-md-6 col-sm-12">
												<div class="inner-box">
													<div class="icon-box">
														<span class="icon flaticon-worker"></span>
													</div>
													<h4>All Expert Engineers</h4>
													<div class="featured-text">Incididunt laboret dolore magna exercitation laboris nisis dolor in derit in voluptate velit.</div>
												</div>
											</div>
											
											<!-- Feature Block -->
											<div class="feature-block col-lg-6 col-md-6 col-sm-12">
												<div class="inner-box">
													<div class="icon-box">
														<span class="icon flaticon-nuclear-plant"></span>
													</div>
													<h4>Power Efficient Factory</h4>
													<div class="featured-text">Incididunt laboret dolore magna exercitation laboris nisis dolor in derit in voluptate velit.</div>
												</div>
											</div>
											
										</div>
									</div>
									
									<!-- Default Two Column -->
									<div class="default-two-column">
										<div class="row clearfix">
											<div class="column col-lg-6 col-md-6 col-sm-12">
												<div class="image">
													<img src="images/resource/service-13.jpg" alt="" />
												</div>
											</div>
											<div class="column col-lg-6 col-md-6 col-sm-12">
												<p>Incididunt ut labore et dolore magna aliqua ut enim trud exercitation ullamco laboris nisi ut aliquip ex ea aute irure dolor in reprehenderit ex bea sed conseq uat ipsum duis sit amet.</p>
												<ul class="list-style-one">
													<li>Leading industrial solutions with machinery</li>
													<li>Voluptatem acusantium doloremque laudant</li>
													<li>Eperiam eaque ipsa quae ab illo inventore</li>
													<li>Quasi architecto beatae vitae dicta sunt</li>
												</ul>
											</div>
										</div>
									</div>
									<h3>Innovative Industrial Technology</h3>
									<p>Auis nostrud exercitation ullamc laboris sed nisit aliquip ex bea sed consequat ipsum duis sit amet consecter adipisicing elit sed ipsum eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adip-isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod consequat. Duis aute irure dolor in reprehe derit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
								</div>
								
								<h5>Our services at a glance ...</h5>
								<!-- Fact Counter -->
								<div class="fact-counter">
									<div class="clearfix">
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2000" data-stop="25">0</span>+
													<h4 class="counter-title">Years Experience</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="2500" data-stop="36">0</span>
													<h4 class="counter-title">Industries Served</h4>
												</div>
											</div>
										</div>
										
										<!--Column-->
										<div class="column counter-column col-lg-4 col-md-4 col-sm-12">
											<div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
												<div class="count-outer count-box">
													<span class="count-text" data-speed="3000" data-stop="105">0</span>
													<h4 class="counter-title">Factories Built</h4>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>