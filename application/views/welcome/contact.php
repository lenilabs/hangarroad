<section class="page-banner" style="background-image:url(assets/images/contact.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Contact us</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url('') ?>">Home</a></li>
                    <li>Get In Touch</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="contact-page-section">
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Info Column -->
				<div class="info-column col-lg-4 col-md-12 col-sm-12">
					<div class="inner-column wow fadeInLeft" data-wow-delay="0ms">
						<!-- Title Box -->
						<div class="title-box">
							<h3>Contact Details</h3>
							<div class="title-text">Get in touch with us for any questions about our industries or projects.</div>
						</div>
						<ul class="contact-info-list">
							<li><span class="icon icon-location-pin"></span><strong>Head Office</strong>2B Lawrence Omole Close Off Sinari Daranijo VI, Lagos.</li>
							<li><span class="icon icon-envelope-open"></span><strong>Email us</strong>info2@hangarroadinternational.com</li>
							<li><span class="icon icon-call-in"></span><strong>Call Us </strong>+234 (01) 2772332</li>
						</ul>
						
						<!-- Social Links -->
						<ul class="social-links">
							<li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
							<li><a href="#"><span class="fab fa-google-plus-g"></span></a></li>
							<li><a href="#"><span class="fab fa-twitter"></span></a></li>
							<li><a href="#"><span class="fab fa-linkedin-in"></span></a></li>
							<li><a href="#"><span class="fab fa-youtube"></span></a></li>
						</ul>
						
					</div>
				</div>
				
				<!-- Form Column -->
				<div class="form-column col-lg-8 col-md-12 col-sm-12">
					<div class="inner-column wow fadeInRight" data-wow-delay="0ms">
						<!-- Sec Title -->
						<div class="sec-title">
							<div class="title">We are Hangar Road International</div>
							<h2>Send us a Message</h2>
						</div>
						
						<!-- Contact Form -->
						<div class="contact-form">
							<?php if($this->session->flashdata('success')): ?>
								<?php $message = $this->session->flashdata('success'); ?>
								<?php 
									$message = $message['message'];
								?>
								
								<div class="alert alert-success" role="alert">
									<h5 id="flexbug-12-inline-elements-arent-treated-as-flex-items">Your Message has been sent successsfully, A representative will be in touch with 24hours</h5>
								</div>
							<?php endif; ?>
							<form method="post" action="<?= base_url('contact') ?>" id="contact-form">
								<div class="row clearfix">
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="firstname" placeholder="First Name " required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="lastname" placeholder="Last Name " required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="email" name="email" placeholder="Email " required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="phone" placeholder="Phone " required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="company" placeholder="Company " required>
									</div>
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group">
										<input type="text" name="subject" placeholder="Subject " required>
									</div>
									
									<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<textarea name="message" placeholder="Message "></textarea>
									</div>
								
									<div class="col-lg-12 col-md-12 col-sm-12 form-group">
										<button class="theme-btn btn-style-five" type="submit" name="submit-form">Send Now</button>
									</div>
									
								</div>
							</form>
								
						</div>
						
					</div>
				</div>
				
			</div>
		</div>
    </section>
    
    <section class="contact-map-section">
        <div class="outer-container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.741512343581!2d3.4319928147702243!3d6.427246095349875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103bf53b5b7b59cb%3A0x1a8a9821e8f0eeac!2s2+Lawrence+Omole+Cl%2C+Victoria+Island%2C+Lagos!5e0!3m2!1sen!2sng!4v1560659589808!5m2!1sen!2sng" width="100%" height="600" style="border:solid 1px #001e57" frameborder="1" style="border:0" allowfullscreen></iframe>
        </div>
    </section>