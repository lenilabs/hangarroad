<section class="page-banner style-two" style="background-image:url(images/background/services-title-bg.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Our industries</h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li>Where We Are</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Services Section Three -->
    <section class="services-section-three style-two">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title">
                <div class="row">
                    <div class="col-lg-4 col-md-12 wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="title">We are Solustrid</div>
                        <h2>We’re Helping Industries</h2>
                    </div>
                    <div class="col-lg-8 col-md-12 wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="text">Aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit voluptate velit sunt culpa quis officia deseru mollit anim ipsum id est laborum sed do eiusmod tempor incididunt.</div>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="building-construction.html"><img src="images/resource/service-2-1.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="building-construction.html">Building & Construction</a></h3>
                            <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div>
                            <a href="building-construction.html" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="chemical-research.html"><img src="images/resource/service-2-2.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="chemical-research.html">Chemical Research</a></h3>
                            <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div>
                            <a href="chemical-research.html" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="oil-gas.html"><img src="images/resource/service-2-3.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="oil-gas.html">Oil & Gas Engineering</a></h3>
                            <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div>
                            <a href="oil-gas.html" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="plants.html"><img src="images/resource/service-2-4.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="plants.html">Petrolium Refinery</a></h3>
                            <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div>
                            <a href="plants.html" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="auto-industry.html"><img src="images/resource/service-2-5.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="auto-industry.html">Automation Industry</a></h3>
                            <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div>
                            <a href="auto-industry.html" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>

                <!-- Services Block Three -->
                <div class="services-block-three col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="image">
                            <a href="auto-industry.html"><img src="images/resource/service-2-6.jpg" alt="" /></a>
                        </div>
                        <div class="lower-content">
                            <h3><a href="auto-industry.html">Mechanical Works</a></h3>
                            <div class="text">Auis nostrud exercitation ullamc laboris nisitm aliquip ex bea sed consequat duis autes ure dolor. dolore magna aliqua nim ad minim.</div>
                            <a href="auto-industry.html" class="read-more">Read More <span class="fas fa-angle-right"></span></a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>