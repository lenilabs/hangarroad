<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title></title>
    <style type="text/css">
        /* CONFIG STYLES Please do not delete and edit CSS styles below */


        /* IMPORTANT THIS STYLES MUST BE ON FINAL EMAIL */

        #outlook a {
            padding: 0;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .es-button {
            mso-style-priority: 100 !important;
            text-decoration: none !important;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .es-desk-hidden {
            display: none;
            float: left;
            overflow: hidden;
            width: 0;
            max-height: 0;
            line-height: 0;
            mso-hide: all;
        }


        /*
        END OF IMPORTANT
        */

        html,
        body {
            width: 100%;
            font-family: lato, 'helvetica neue', helvetica, arial, sans-serif;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            border-collapse: collapse;
            border-spacing: 0px;
        }

        table td,
        html,
        body,
        .es-wrapper {
            padding: 0;
            Margin: 0;
        }

        .es-content,
        .es-header,
        .es-footer {
            table-layout: fixed !important;
            width: 100%;
        }

        img {
            display: block;
            border: 0;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        table tr {
            border-collapse: collapse;
        }

        p,
        hr {
            Margin: 0;
        }

        h1,
        h2,
        h3,
        h4,
        h5 {
            Margin: 0;
            line-height: 120%;
            mso-line-height-rule: exactly;
            font-family: lato, 'helvetica neue', helvetica, arial, sans-serif;
        }

        p,
        ul li,
        ol li,
        a {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            mso-line-height-rule: exactly;
        }

        .es-left {
            float: left;
        }

        .es-right {
            float: right;
        }

        .es-p5 {
            padding: 5px;
        }

        .es-p5t {
            padding-top: 5px;
        }

        .es-p5b {
            padding-bottom: 5px;
        }

        .es-p5l {
            padding-left: 5px;
        }

        .es-p5r {
            padding-right: 5px;
        }

        .es-p10 {
            padding: 10px;
        }

        .es-p10t {
            padding-top: 10px;
        }

        .es-p10b {
            padding-bottom: 10px;
        }

        .es-p10l {
            padding-left: 10px;
        }

        .es-p10r {
            padding-right: 10px;
        }

        .es-p15 {
            padding: 15px;
        }

        .es-p15t {
            padding-top: 15px;
        }

        .es-p15b {
            padding-bottom: 15px;
        }

        .es-p15l {
            padding-left: 15px;
        }

        .es-p15r {
            padding-right: 15px;
        }

        .es-p20 {
            padding: 20px;
        }

        .es-p20t {
            padding-top: 20px;
        }

        .es-p20b {
            padding-bottom: 20px;
        }

        .es-p20l {
            padding-left: 20px;
        }

        .es-p20r {
            padding-right: 20px;
        }

        .es-p25 {
            padding: 25px;
        }

        .es-p25t {
            padding-top: 25px;
        }

        .es-p25b {
            padding-bottom: 25px;
        }

        .es-p25l {
            padding-left: 25px;
        }

        .es-p25r {
            padding-right: 25px;
        }

        .es-p30 {
            padding: 30px;
        }

        .es-p30t {
            padding-top: 30px;
        }

        .es-p30b {
            padding-bottom: 30px;
        }

        .es-p30l {
            padding-left: 30px;
        }

        .es-p30r {
            padding-right: 30px;
        }

        .es-p35 {
            padding: 35px;
        }

        .es-p35t {
            padding-top: 35px;
        }

        .es-p35b {
            padding-bottom: 35px;
        }

        .es-p35l {
            padding-left: 35px;
        }

        .es-p35r {
            padding-right: 35px;
        }

        .es-p40 {
            padding: 40px;
        }

        .es-p40t {
            padding-top: 40px;
        }

        .es-p40b {
            padding-bottom: 40px;
        }

        .es-p40l {
            padding-left: 40px;
        }

        .es-p40r {
            padding-right: 40px;
        }

        .es-menu td {
            border: 0;
        }

        .es-menu td a img {
            display: inline !important;
        }


        /* END CONFIG STYLES */

        a {
            font-family: lato, 'helvetica neue', helvetica, arial, sans-serif;
            font-size: 14px;
            text-decoration: underline;
        }

        h1 {
            font-size: 48px;
            font-style: normal;
            font-weight: normal;
            color: #000;
        }

        h1 a {
            font-size: 48px;
        }

        h2 {
            font-size: 24px;
            font-style: normal;
            font-weight: normal;
            color: #000;
        }

        h2 a {
            font-size: 24px;
        }

        h3 {
            font-size: 20px;
            font-style: normal;
            font-weight: normal;
            color: #000;
        }

        h3 a {
            font-size: 20px;
        }

        p,
        ul li,
        ol li {
            font-size: 14px;
            font-family: lato, 'helvetica neue', helvetica, arial, sans-serif;
            line-height: 200%;
        }

        ul li,
        ol li {
            Margin-bottom: 15px;
        }

        .es-menu td a {
            text-decoration: none;
            display: block;
        }

        .es-wrapper {
            width: 100%;
            height: 100%;
            background-image: ;
            background-repeat: repeat;
            background-position: center top;
        }

        .es-wrapper-color {
            background-color: #f4f4f4;
        }

        .es-content-body {
            background-color: #ffffff;
        }

        .es-content-body p,
        .es-content-body ul li,
        .es-content-body ol li {
            color: #000;
        }

        .es-content-body a {
            color: #136ACD;
        }

        .es-header {
            background-color: #136ACD;


            background-repeat: repeat;
            background-position: center top;
        }

        .es-header-body {
            background-color: transparent;
        }

        .es-header-body p,
        .es-header-body ul li,
        .es-header-body ol li {
            color: #000;
            font-size: 12px;
        }

        .es-header-body a {
            color: #000;
            font-size: 14px;
        }

        .es-footer {
            background-color: transparent;
            background-image: ;
            background-repeat: repeat;
            background-position: center top;
        }

        .es-footer-body {
            background-color: transparent;
        }

        .es-footer-body p,
        .es-footer-body ul li,
        .es-footer-body ol li {
            color: #000;
            font-size: 14px;
        }

        .es-footer-body a {
            color: #000;
            font-size: 14px;
        }

        .es-infoblock,
        .es-infoblock p,
        .es-infoblock ul li,
        .es-infoblock ol li {
            line-height: 120%;
            font-size: 12px;
            color: #cccccc;
        }

        .es-infoblock a {
            font-size: 12px;
            color: #cccccc;
        }

        a.es-button {
            border-style: solid;
            border-color: #136ACD;
            border-width: 15px 25px 15px 25px;
            display: inline-block;
            background: #136ACD;
            border-radius: 2px;
            font-size: 20px;
            font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;
            font-weight: normal;
            font-style: normal;
            line-height: 120%;
            color: #ffffff;
            text-decoration: none;
            width: auto;
            text-align: center;
        }

        .es-button-border {
            border-style: solid solid solid solid;
            border-color: #136ACD #136ACD #136ACD #136ACD;
            background: 1px;
            border-width: 1px 1px 1px 1px;
            display: inline-block;
            border-radius: 2px;
            width: auto;
        }


        /* RESPONSIVE STYLES Please do not delete and edit CSS styles below. If you don't need responsive layout, please delete this section. */

        @media only screen and (max-width: 600px) {
            p,
            ul li,
            ol li,
            a {
                font-size: 16px !important;
                line-height: 150% !important;
            }
            h1 {
                font-size: 30px !important;
                text-align: center;
                line-height: 120% !important;
            }
            h2 {
                font-size: 26px !important;
                text-align: center;
                line-height: 120% !important;
            }
            h3 {
                font-size: 20px !important;
                text-align: center;
                line-height: 120% !important;
            }
            h1 a {
                font-size: 30px !important;
            }
            h2 a {
                font-size: 26px !important;
            }
            h3 a {
                font-size: 20px !important;
            }
            .es-menu td a {
                font-size: 16px !important;
            }
            .es-header-body p,
            .es-header-body ul li,
            .es-header-body ol li,
            .es-header-body a {
                font-size: 16px !important;
            }
            .es-footer-body p,
            .es-footer-body ul li,
            .es-footer-body ol li,
            .es-footer-body a {
                font-size: 16px !important;
            }
            .es-infoblock p,
            .es-infoblock ul li,
            .es-infoblock ol li,
            .es-infoblock a {
                font-size: 12px !important;
            }
            *[class="gmail-fix"] {
                display: none !important;
            }
            .es-m-txt-c,
            .es-m-txt-c h1,
            .es-m-txt-c h2,
            .es-m-txt-c h3 {
                text-align: center !important;
            }
            .es-m-txt-r,
            .es-m-txt-r h1,
            .es-m-txt-r h2,
            .es-m-txt-r h3 {
                text-align: right !important;
            }
            .es-m-txt-l,
            .es-m-txt-l h1,
            .es-m-txt-l h2,
            .es-m-txt-l h3 {
                text-align: left !important;
            }
            .es-m-txt-r img,
            .es-m-txt-c img,
            .es-m-txt-l img {
                display: inline !important;
            }
            .es-button-border {
                display: block !important;
            }
            a.es-button {
                font-size: 20px !important;
                display: block !important;
                border-width: 15px 25px 15px 25px !important;
            }
            .es-btn-fw {
                border-width: 10px 0px !important;
                text-align: center !important;
            }
            .es-adaptive table,
            .es-btn-fw,
            .es-btn-fw-brdr,
            .es-left,
            .es-right {
                width: 100% !important;
            }
            .es-content table,
            .es-header table,
            .es-footer table,
            .es-content,
            .es-footer,
            .es-header {
                width: 100% !important;
                max-width: 600px !important;
            }
            .es-adapt-td {
                display: block !important;
                width: 100% !important;
            }
            .adapt-img {
                width: 100% !important;
                height: auto !important;
            }
            .es-m-p0 {
                padding: 0px !important;
            }
            .es-m-p0r {
                padding-right: 0px !important;
            }
            .es-m-p0l {
                padding-left: 0px !important;
            }
            .es-m-p0t {
                padding-top: 0px !important;
            }
            .es-m-p0b {
                padding-bottom: 0 !important;
            }
            .es-m-p20b {
                padding-bottom: 20px !important;
            }
            .es-mobile-hidden,
            .es-hidden {
                display: none !important;
            }
            .es-desk-hidden {
                display: table-row!important;
                width: auto!important;
                overflow: visible!important;
                float: none!important;
                max-height: inherit!important;
                line-height: inherit!important;
            }
            .es-desk-menu-hidden {
                display: table-cell!important;
            }
            table.es-table-not-adapt,
            .esd-block-html table {
                width: auto !important;
            }
            table.es-social {
                display: inline-block !important;
            }
            table.es-social td {
                display: inline-block !important;
            }
        }


        /* END RESPONSIVE STYLES */

        .es-p-default {
            padding-top: 20px;
            padding-right: 30px;
            padding-bottom: 0px;
            padding-left: 30px;
        }

        .es-p-all-default {
            padding: 0px;
        }
    </style>
</head>

<body>
    <div class="es-wrapper-color">
        <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td class="esd-email-paddings" valign="top">
                        <table class="esd-header-popover es-content" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" style="background-color: rgb(250, 250, 250);background:#FAFAFA;" align="center">
                                        <table class="es-content-body" style="background-color: rgb(250, 250, 250);background:#FAFAFA;" width="600" cellspacing="0" cellpadding="0" bgcolor="#fafafa" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p20t es-p5b es-p20r es-p20l" style="background-position: left top;" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td esd-links-color="#cccccc" class="esd-block-text es-p5b es-infoblock" esd-links-underline="none" align="center">
                                                                                        <p> <a target="_blank" href style="text-decoration: none;">View in browser</a></p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" style="background-color: rgb(250, 250, 250);" bgcolor="#fafafa" align="center">
                                        <table class="es-content-body" style="background-color: rgb(255, 255, 255);" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                            <tbody>
                                                <tr style="">
                                                    <td class="esd-structure es-p10t es-p15b es-p20r es-p20l" style="height:90px;background-color:#FFF;background-size:cover;border-bottom:solid 2px #8CBF1F; border-radius: 10px 10px 0 0px; background-position: center;background-repeat:no-repeat;" bgcolor="#0b5394" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr >
                                                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr >
                                                                                    <td class="esd-block-image es-p10t"  align="center">
                                                                                        <a target="_blank" href> <img src="https://hangarroadinternational.com/assets/images/hangarroad.png" width="120" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-structure es-p40t es-p20r es-p20l" style="background-color: transparent; background-position: left top;" bgcolor="transparent" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="560" valign="top" align="left">
                                                                        <table style="background-position: left top;" width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>                                                                               
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p20r es-p20l" align="left">
                                                                                        <p style="text-align: left;">Hello <strong> hangarroadinternational</strong> </p>

                                                                                        <p>You have a new contact request.</p>

                                                                                        <p>You can log in with the following credentials.</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p20r es-p20l" align="left">
                                                                                        <p style="text-align: left;"><span style="width:300px; font-weight:bolder;">Firstname </span>: <?php echo $firstname ?></p>
                                                                                        <p style="text-align: left;"><span style="width:300px; font-weight:bolder;">Lastname </span>: <?php echo $lastname ?></p>
                                                                                        <p style="text-align: left;"><span style="width:300px; font-weight:bolder;">Email </span>: <?php echo $email ?></p>
                                                                                        <p style="text-align: left;"><span style="width:300px; font-weight:bolder;">Phone </span>: <?php echo $phone ?></p>
                                                                                        <p style="text-align: left;"><span style="width:300px; font-weight:bolder;">Company </span>: <?php echo $company ?></p>
                                                                                        <p style="text-align: left;"><span style="width:300px; font-weight:bolder;">Subject </span>: <?php echo $subject ?></p>
                                                                                        <p style="text-align: left;"><span style="width:300px; font-weight:bolder;">Message </span>: <?php echo $message ?></p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p25t es-p40r es-p20l es-p30b" align="left">
                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="background-color:#8CBF1F;height:43px;background-size:cover;background-repeat:no-repeat;">
                                                    <td class="esd-block-text es-p25t es-p40r es-p40l" align="center">
                                                        <p></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-footer" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" style="background-color: rgb(250, 250, 250);" bgcolor="#fafafa" align="center">
                                        <table class="es-footer-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" bgcolor="transparent" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p15t es-p5b es-p20r es-p20l" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text" esd-links-underline="underline" align="center">
                                                                                        <p style="font-size: 12px; color: #666666;">if you have reason to suspect any unauthorised activity on your account please contact us by sending an email to <a href="#" target="_blank" style="font-size: 12px; text-decoration: underline;">info2@hangarroadinternational.com</a>.</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content esd-footer-popover" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" esd-custom-block-id="42537" align="center">
                                        <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p30t es-p30b es-p20r es-p20l" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="560" valign="top" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-infoblock" align="center">
                                                                                        <p style="font-size: 12px; color: #666666;">Copyright &copy; <?= date('Y') ?>. hangarroadinternational</p>                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>