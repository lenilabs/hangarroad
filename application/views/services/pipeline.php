<section class="page-banner" style="background-image:url(<?= base_url('assets/images/background/pipeline_maintenance.jpg') ?>);background-position:25% 60%">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>Pipeline Maintenance </h1>
                <ul class="bread-crumb clearfix">
                    <li><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="javascript:;">Services</a></li>
                    <li><a href="<?= base_url('services/facility') ?>">Facility Maintenance</a></li>
                    <li>Pipeline Maintenance </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
	
	<!--Sidebar Page Container-->
    
	
	<!--Newsleter Section-->
    <div class="sidebar-page-container">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
				<!--Sidebar Side-->
                
                <?php $this->load->view('services/services_nav') ?>
				
				<!--Content Side / Services Detail-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                	<div class="services-detail">
						<div class="inner-box">
							
							<div class="lower-content">
								<!-- Title Box -->
								<div class="title-box">
									<div class="title">Hangar Road International</div>
									<h2>Pipeline Maintenance</h2>
                                </div>
                                <div class="bold-text">The efficient operation of pipelines is crucial to any asset owner and maintaining a clean pipeline not only helps maximise flow rates but increases the longevity of the asset, improves reliability and safety and reduces risk. At HRI, our experienced pipelines team will ensure your pipelines continue to operate at maximum efficiency with minimal downtime. Our specialist cleaning methods will ensure debris removal with minimal cost and disruption.</div>

								<div class="bold-text">HRI provides a wide range of maintenance solutions designed to address common problems including:</div>
                                <div class="text">
                                    <div class="default-two-column">
                                        <div class="row clearfix">
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <ul class="list-style-one">
                                                    <li>Pipeline Chemical Cleaning</li>
                                                    <li>Pig Tracking</li>
                                                    <li>Pipeline Gauging </li>
                                                    <li>Hydrostatic Testing </li>
                                                    <li>Dewatering </li>
                                                    <li>Nitrogen Drying </li>
                                                    <li>Nitrogen Packing</li>
                                                </ul>
                                            </div>
                                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                                <ul class="list-style-one">
                                                    <li>Pipeline Calliper and ILI inspection</li>
                                                    <li>Pipeline Flooding </li>
                                                    <li>Pipeline Cleaning</li>
                                                    <li>Leak Testing</li>
                                                    <li>Air Drying </li>
                                                    <li>Vacuum Drying </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>