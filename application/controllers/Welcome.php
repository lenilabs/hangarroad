<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function about()
	{

	}

	public function contact()
	{
		$data = $this->input->post();
		if($data) {
			//data is sent capture form variables ans send email
			$email_view = "emails/email";
			$this->sendEmail("info2@hangarroadinternational.com", "NEW CONTACT REQUEST",$email_view, $data);

			$this->session->set_flashdata('success', ["message" => "Success Your Email has Been successfully sent"]);
		}
	}
	
}
